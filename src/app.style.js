// @flow
const styles = (theme: Object): Object => ({
  wrapper: {
    width: '450px',
    padding: '0px 20px',
  },
});

export default styles;
