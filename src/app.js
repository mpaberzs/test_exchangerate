// @flow
import React, { type Node } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from '@material-ui/core/styles';
import ErrorModal from './components/error-modal';
import TopControls from './components/top-controls';
import DataContainer from './components/data-container';
import styles from './app.style';

type PropTypes = {|
  loaded: boolean,
  init: boolean,
  classes: Object,
  data: Object,
  dispatch: Function,
|};

const App = ({ data, loaded, init, classes, dispatch }: PropTypes): Node => {
  const buttonAction = () => {
    dispatch({ type: 'DATA_FETCH_START' });
  };

  return (
    <div className={classes.wrapper} data-name="main-wrapper">
      <h1>Exchange rates information</h1>
      <TopControls {...{ loaded, init, buttonAction }} />
      {init && !loaded && (
        <div className={classes.loaderWrap} data-name="loader-wrapper">
          <CircularProgress />
        </div>
      )}
      <DataContainer data={data} />
      <ErrorModal />
    </div>
  );
};

export default compose(
  withStyles(styles),
  connect(state => ({
    loaded: state.main.loaded,
    init: state.main.init,
    data: state.main.data,
  })),
)(App);
