// @flow
export const data = {
  date: '2019-01-23',
  base: 'EUR',
  rates: {
    EUR: 1,
    USD: 2,
  },
};

export const invalidData = {
  ...data,
  date: '',
};
