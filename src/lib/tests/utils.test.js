import * as utils from '../utils';
import * as mocks from './mocks';

describe('Utilities functions:', () => {
  describe('getFormattedDate utility', () => {
    it('should return proper date string if correct data passed', () => {
      expect(utils.getFormattedDate(mocks.data)).toMatchSnapshot();
    });

    it('should return empty string if invalid or no data passed', () => {
      expect(utils.getFormattedDate()).toBe('');
      expect(utils.getFormattedDate(null)).toBe('');
      expect(utils.getFormattedDate({})).toBe('');
      expect(utils.getFormattedDate(mocks.invalidData)).toBe('');
    });
  });

  describe('fetchData utility', () => {
    beforeAll(() => {
      global.fetch = require('jest-fetch-mock');
    });

    beforeEach(() => {
      fetch.resetMocks();
    });

    it("should call global 'fetch'", () => {
      fetch.mockResponseOnce(JSON.stringify(mocks.data), { status: 200 });
      expect(
        utils
          .fetchData()
          .then(() => expect(fetch.mock.calls.length).toEqual(1)),
      );
    });

    it('should return proper data object if all good', () => {
      fetch.mockResponseOnce(JSON.stringify(mocks.data), { status: 200 });
      expect.assertions(1);
      expect(utils.fetchData()).resolves.toEqual(mocks.data);
    });

    it('should throw an error if bad response', () => {
      fetch.mockResponseOnce({}, { status: 500 });
      expect.assertions(1);
      expect(
        utils
          .fetchData()
          .catch(err =>
            expect(err).toEqual(Error('Failed to fetch the data!')),
          ),
      );
    });
  });
});
