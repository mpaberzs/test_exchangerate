// @flow
export type InitialState = {|
  data: Object | null,
  loaded: boolean,
  init: boolean,
  error: Error | null,
  showErrorModal: boolean,
|};

export type Action =
  | {| type: 'DATA_FETCH_START' |}
  | {| type: 'DATA_FETCH_SUCCESS', payload: {| data: Object |} |}
  | {| type: 'DATA_FETCH_ERROR', payload: {| error: Error |} |}
  | {| type: 'HIDE_ERROR_MODAL' |};
