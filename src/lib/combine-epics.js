// @flow
import { combineEpics } from 'redux-observable';
import { dataFetchEpic } from './epics';

const rootEpic = combineEpics(dataFetchEpic);

export default rootEpic;
