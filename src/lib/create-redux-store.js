import { createStore, applyMiddleware } from 'redux';
import rootEpic from './combine-epics';
import { epicMiddleware, loggerMiddleware } from './create-redux-middleware';
import rootReducer from './combine-reducers';

const store = createStore(
  rootReducer,
  applyMiddleware(loggerMiddleware, epicMiddleware),
);

epicMiddleware.run(rootEpic);

export default store;
