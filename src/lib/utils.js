// @flow
export const fetchData = (): Promise<any> => {
  try {
    return fetch('https://api.exchangeratesapi.io/latest').then(res => {
      if (res.status !== 200) {
        throw new Error('Failed to fetch the data!');
      }

      return res.json();
    });
  } catch (err) {
    throw err;
  }
};

export const getFormattedDate = (data: Object | null): string => {
  let date = '';

  if (data && data.date && typeof data.date === 'string') {
    const dateObj = new Date(data.date);

    if (dateObj.toString() !== 'Invalid Date') {
      date = dateObj.toDateString();
    }
  }

  return date;
};
