// @flow
import { createLogger } from 'redux-logger';
import { createEpicMiddleware } from 'redux-observable';

export const loggerMiddleware = createLogger();

export const epicMiddleware = createEpicMiddleware();
