// @flow
import type { Action, InitialState } from './types';

const initialState: InitialState = {
  data: null,
  loaded: false,
  init: false,
  error: null,
  showErrorModal: false,
};

export default function reducer(
  state: InitialState = initialState,
  action: Action,
) {
  switch (action.type) {
    case 'DATA_FETCH_START':
      return { ...state, init: true, loaded: false, showErrorModal: false };

    case 'DATA_FETCH_SUCCESS':
      return { ...state, data: action.payload.data, loaded: true };

    case 'DATA_FETCH_ERROR':
      return {
        ...state,
        loaded: true,
        showErrorModal: true,
        error: action.payload.error,
      };

    case 'HIDE_ERROR_MODAL':
      return { ...state, showErrorModal: false };

    default:
      return state;
  }
}
