import { from, of } from 'rxjs';
import { map, mergeMap, catchError } from 'rxjs/operators';
import { ofType } from 'redux-observable';
import { fetchData } from './utils';

export const dataFetchEpic = action$ =>
  action$.pipe(
    ofType('DATA_FETCH_START'),
    mergeMap(() =>
      from(fetchData()).pipe(
        map(data => ({ type: 'DATA_FETCH_SUCCESS', payload: { data } })),
        catchError(error =>
          of({ type: 'DATA_FETCH_ERROR', payload: { error } }),
        ),
      ),
    ),
  );
