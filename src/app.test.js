import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import App from './app';

describe('<App />', () => {
  let shallow;
  beforeEach(() => {
    shallow = createShallow({ dive: true });
  });

  it('renders without crashing', () => {
    shallow(<App />);
  });
});
