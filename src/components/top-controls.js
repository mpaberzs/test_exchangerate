// @flow
import React, { type Node } from 'react';
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import styles from './top-controls.style';

type PropTypes = {|
  loaded: boolean,
  init: boolean,
  buttonAction: Function,
  classes: Object,
|};

const TopControls = ({
  loaded,
  init,
  buttonAction,
  classes,
}: PropTypes): Node => {
  const buttonDisabled = !loaded && init;

  return (
    <div className={classes.wrapper}>
      <Button
        variant="contained"
        color="primary"
        disabled={buttonDisabled}
        onClick={buttonAction}
        id="top-controls-button"
      >
        Fetch data
      </Button>
    </div>
  );
};

export default withStyles(styles)(TopControls);
