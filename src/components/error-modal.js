// @flow
import React, { type Node } from 'react';
import { compose } from 'redux';
import { connect } from 'react-redux';
import Modal from '@material-ui/core/Modal';
import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';
import styles from './error-modal.style';

type PropTypes = {|
  showErrorModal: boolean,
  dispatch: Function,
  classes: Object,
  error: Error,
|};

const ErrorModal = ({
  showErrorModal,
  dispatch,
  classes,
  error,
}: PropTypes): Node => {
  const onModalClose = () => {
    dispatch({ type: 'HIDE_ERROR_MODAL' });
  };

  return (
    <Modal open={showErrorModal} onClose={onModalClose}>
      <div className={classes.paper}>
        <div className={classes.messageWrap}>
          <Icon color="error" className={classes.icon}>
            highlight_off
          </Icon>
          {error && <span>{error.toString()}</span>}
        </div>
      </div>
    </Modal>
  );
};

export default compose(
  withStyles(styles),
  connect(state => ({
    error: state.main.error,
    showErrorModal: state.main.showErrorModal,
  })),
)(ErrorModal);
