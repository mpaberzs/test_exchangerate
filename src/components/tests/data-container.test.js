import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import DataContainer from '../data-container';

const data = {
  date: '2019-01-23',
  base: 'EUR',
  rates: {
    EUR: 1,
    USD: 2,
  },
};

describe('<DataContainer />', () => {
  let shallow;
  beforeEach(() => {
    shallow = createShallow({
      dive: 'true',
    });
  });

  it('renders without crashing', () => {
    shallow(<DataContainer data={data} />);
  });

  it('matches snapshot', () => {
    const wrapper = shallow(<DataContainer data={data} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders wrapper and both headings if data is there', () => {
    const wrapper = shallow(<DataContainer data={data} />);
    expect(wrapper).toContainMatchingElement('div');
    expect(wrapper).toContainMatchingElements(2, 'h3');
  });

  it('dont render anything if no data', () => {
    const wrapper = shallow(<DataContainer />);
    expect(wrapper).toBeEmptyRender();
  });
});
