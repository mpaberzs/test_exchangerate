import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import TopControls from '../top-controls';

describe('<TopControls />', () => {
  let shallow;
  beforeEach(() => {
    shallow = createShallow({
      dive: 'true',
    });
  });

  it('Renders without crashing', () => {
    shallow(<TopControls />);
  });

  it('matches snapshot', () => {
    const wrapper = shallow(<TopControls {...{ loaded: false, init: true }} />);

    expect(wrapper).toMatchSnapshot();
  });

  it('Button is not disabled initially', () => {
    const wrapper = shallow(
      <TopControls {...{ loaded: false, init: false }} />,
    );

    expect(wrapper.find('#top-controls-button')).toHaveProp({
      disabled: false,
    });
  });

  it('Button is disabled when loading', () => {
    const wrapper = shallow(<TopControls {...{ loaded: false, init: true }} />);

    expect(wrapper.find('#top-controls-button')).toHaveProp({
      disabled: true,
    });
  });
});
