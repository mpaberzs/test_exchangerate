import React from 'react';
import { createShallow } from '@material-ui/core/test-utils';
import ErrorModal from '../error-modal';

describe('<ErrorModal />', () => {
  let shallow;
  beforeEach(() => {
    shallow = createShallow({
      dive: 'true',
    });
  });

  it('Renders without crashing', () => {
    shallow(<ErrorModal />);
  });

  it('matches snapshot', () => {
    const wrapper = shallow(<ErrorModal />);
    expect(wrapper).toMatchSnapshot();
  });
});
