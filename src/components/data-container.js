// @flow
import React, { type Node } from 'react';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import { getFormattedDate } from '../lib/utils';

type PropTypes = {|
  classes: Object,
  data: Object | null,
|};

const styles = theme => ({
  wrapper: {
    marginBottom: '30px',
  },
  tableRow: {
    '&:nth-of-type(even)': { backgroundColor: 'rgba(0,0,0,0.05)' },
  },
});

const DataContainer = ({ data, classes }: PropTypes): Node => {
  const formattedDate = getFormattedDate(data);

  let currencies = [];
  if (data && data.rates) {
    currencies = Object.keys(data.rates);
  }

  return data ? (
    <div data-name="data-container-wrapper" className={classes.wrapper}>
      {formattedDate && <h3>Updated: {formattedDate}</h3>}
      {data.base && <h3>Base: {data.base}</h3>}
      {currencies && currencies.length && (
        <Table className={classes.table}>
          <TableHead>
            <TableRow>
              <TableCell>currency</TableCell>
              <TableCell>rate</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {currencies.map(curr => (
              <TableRow key={curr} className={classes.tableRow}>
                <TableCell>{curr}</TableCell>
                <TableCell>{data.rates[curr]}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      )}
    </div>
  ) : null;
};

export default withStyles(styles)(DataContainer);
